# gke-simple-ci

### Gitlab AutoDevops based ci/cd, for multi stage environment in a single GKE cluster

This setup is for those who wants to quickly setup minimalistic Gitlab CI/CD  
for microservice architecture based project, without deep learning into kubernetes and GKE administration
details, and also digging into Gitlab CI scripting. 

### Result 
Development and production environments running the same GKE cluster, separated by different node pools.
Possibility to start production environment any time.

1. Helm chart is capable to automatically populate container environment with the variables defined in the same named 
(same as environment) text file in your project.   
2. Helm chart is capable to quickly setup CI for the microservice project, one of the 3 types:
    1. REST API backend, microservice, serving the dedicated API branch with predefined url prefix
       for example https://api.endpoint_dn/service_api_prefix_path/path_segment1,       
       https://api.endpoint_dn/service_api_prefix_path/path_segment1/path_segment2...,
       There will be automatically setup rewrite rule to avoid 
       handling of the service api prefix path inside microservice.
    2. Front end
       http://frontend_l3dn.endpoint_dn
    3. headless microservice for internal purposes which is not exposed to outside.  

3. Access environments following the naming scheme:

    * **REST API backends** 
       * prod: https://api.endpoint_dn/service_api_prefix_path
       * dev: http://api.dev.endpoint_dn/prefix_path
       * review branch: http://branch_or_tag_name.dev.endpoint_dn/prefix_path
    * **Front end backends**: 
       * prod: http://frontend_l3dn.endpoint_dn
       * dev: http://frontend_l3dn.dev.endpoint_dn
       * review branch: http://branch_or_tag_name.frontend_l3dn.dev.endpoint_dn  

4. Automatically enable TLS for production environment. 
5. You will be charged only for GKE cluster running node VMs

<!--    | service type        | prod | dev | review branch |
    | :------------------ | :--- | :--- | ------------ |
    | REST API backends   | https://api.endpoint_dn/prefix_path | http://api.dev.endpoint_dn/prefix_path | http://branch_or_tag_name.dev.endpoint_dn/prefix_path              |
-->

### Pre requirements

1. GCP project created. [Follow this giude to create one](https://cloud.google.com/appengine/docs/standard/nodejs/building-app/creating-project)
2. Running GKE cluster. If not have one it is easy to create one from GCP console. Or you can use the following 
    gcloud command:
    ```shell script
    gcloud container clusters create [YOUR CLUSTER NAME] \
        --machine-type=g1-small --disk-size=20GB --enable-autoscaling \ 
        --max-nodes=3 --min-nodes=1 --num-nodes=1
    ``` 
    One thing you need to keep in mind when you setup 
    cluster do not use micro node type, cluster will not work correctly. 
    Use small machine type if you need to get it most cheaper.      

3. Wildcard domain name pointer 
*.endpoint_dn . You should use you domain name registrar interface to create one
4. Free Gitlab account.
5. This is optional, but it is useful to have gcloud command line utilities installed and configured. Follow links at the bottom of the page https://cloud.google.com/sdk/

### Step by step guide

1. Create cluster endpoint and Install nginx-ingress 
2. Create Gitlab project group
3. Attach GKE cluster to Gitlab Project Group
4. Install Gitlab runner into your cluster. Setup runners resources
4. Deploy test project to the development environment









